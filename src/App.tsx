import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { HashRouter, Route, Switch } from 'react-router-dom';

// Styles
// Import Flag Icons Set
import 'flag-icon-css/css/flag-icon.min.css';
// Import Font Awesome Icons Set
import 'font-awesome/css/font-awesome.min.css';
// Import Simple Line Icons Set
import 'simple-line-icons/css/simple-line-icons.css';
// Import Main styles for this application
import '../scss/style.scss';
// Temp fix for reactstrap
import '../scss/core/_dropdown-menu-right.scss';

interface Props {}
// Containers
import Full from './pages/Full';
import Login from  './pages/Login'

const App = ({}: Props) => {
  return (
    <HashRouter>
    <Switch>
    <Route exact={true} path="/login" name="Login" component={Login}/>
      <Route path="/" name="Home" component={Full}/>
    </Switch>
  </HashRouter>
  )
};

ReactDOM.render(<App />, document.getElementById('app'));