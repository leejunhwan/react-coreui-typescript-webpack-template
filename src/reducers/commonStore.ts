import {combineReducers, createStore} from 'redux';
import {ACTION_TYPE, IActionData} from './actions';

const initCommonData: any = {};

const commonStatus = (state = initCommonData, action : IActionData): any => {
    if (action.type === ACTION_TYPE.COMMON_DATA) {
        if (action.name && action.data) {
            state[action.name] = action.data;
        }
    }
    return state;
};

const reducerData = combineReducers({commonStatus});

export function commonStore(): any {
    let ocbCommonStore = (window as any).ocbCommonStore;
    if (!ocbCommonStore) {
        ocbCommonStore = createStore(reducerData);
    }
    return ocbCommonStore;
}

export function getCommonInfo(name : string) {
    const item = commonStore()
        .getState()
        .commonStatus;
    if (item && item[name]) {
        return item[name];
    }
    return null;
}

export function setCommonInfo(name : string, data : any = null) {
    commonStore().dispatch({type: ACTION_TYPE.COMMON_DATA, name, data});
}

export function removeCommonInfo(name : string) {
    const item = commonStore()
        .getState()
        .commonStatus;
    if (item && item[name]) {
        delete item[name];
    }
}

export function getUserInfo() {
    let loginInfo = sessionStorage.getItem('admin.login');
    if (loginInfo) {
        return JSON.parse(loginInfo);
    }
    return loginInfo;
}

export function isLogin() {
    return !!sessionStorage.getItem('admin.login');
}

export function setUserInfo(userInfo) {
    sessionStorage.setItem('admin.login', JSON.stringify(userInfo));
}

export function setLogout() {
    sessionStorage.removeItem('admin.login');
}
