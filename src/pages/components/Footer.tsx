import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className="app-footer">
        <span>POINT HUB ADMIN &copy; 2020 SK Planet.</span>
      </footer>
    )
  }
}

export default Footer;
