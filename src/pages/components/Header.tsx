import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
    Nav,
    NavbarBrand,
    NavbarToggler,
    NavItem,
    NavLink,
    Badge,
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem
} from 'reactstrap';
import {getUserInfo} from '../../reducers/commonStore';

interface IStateHeader {
    dropdownOpen?: boolean;
}

class Header extends Component < {},
IStateHeader > {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        this.setState({dropdownOpen: false});
    }

    sidebarToggle(e) {
        e.preventDefault();
        document
            .body
            .classList
            .toggle('sidebar-hidden');
    }

    sidebarMinimize(e) {
        e.preventDefault();
        document
            .body
            .classList
            .toggle('sidebar-minimized');
    }

    mobileSidebarToggle(e) {
        e.preventDefault();
        document
            .body
            .classList
            .toggle('sidebar-mobile-show');
    }

    render() {
        const {userId, userInfo} : any = getUserInfo() || {};

        return (
            <header className="app-header navbar">
                <NavbarToggler className="d-lg-none" onClick={this.mobileSidebarToggle}>
                    <span className="navbar-toggler-icon"/>
                </NavbarToggler>
                <NavbarBrand href="#"/>
                <NavbarToggler className="d-md-down-none" onClick={this.sidebarToggle}>
                    <span className="navbar-toggler-icon"/>
                </NavbarToggler>
                <Nav className="ml-auto" navbar={true}>
                    <Dropdown
                        nav={true}
                        isOpen={this.state.dropdownOpen}
                        toggle={() => this.setState({
                            dropdownOpen: !this.state.dropdownOpen
                        })}>
                        <DropdownToggle nav={true}>
                            <img
                                src={'http://jira.skplanet.com/secure/useravatar?ownerId=' + userId}
                                className="img-avatar"/>
                            <span className="d-none d-lg-inline d-xl-inline">{userInfo}&nbsp;&nbsp;</span>
                        </DropdownToggle>
                        <DropdownMenu right={true}>
                            <DropdownItem header={true} tag="div" className="text-center">
                                <strong>Settings</strong>
                            </DropdownItem>
                            <DropdownItem>
                                <NavLink href="#/logout"><i className="fa fa-lock"/>
                                    &nbsp;로그아웃</NavLink>
                            </DropdownItem>
                        </DropdownMenu>
                    </Dropdown>
                </Nav>
            </header>
        );
    }
}

export default Header;
