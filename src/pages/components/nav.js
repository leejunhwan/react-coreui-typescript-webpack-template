export default {
    items: [
      {
        name: "제휴사 관리",
        url: "/affiliate",
        icon: "fa fa-sticky-note-o"
      },
      {
        name: "통계관리",
        url: "/statistic",
        icon: "fa fa-sticky-note-o"
      },
      {
        name: "사용자 관리",
        url: "/member",
        icon: "fa fa-user-o",
        userLevel: 3
      }
    ]
  };
  