import React, {Component} from "react";
import {Redirect} from "react-router-dom";
import {
    Container,
    Row,
    Col,
    CardGroup,
    Card,
    CardBody,
    Button,
    Input,
    InputGroup,
    InputGroupAddon,
    InputGroupText
} from "reactstrap";

import {setUserInfo, isLogin} from "../reducers/commonStore";

interface ILoginState {
    isLogin: boolean
};

class Login extends Component < {},
ILoginState > {
    private userId: HTMLInputElement = null;
    private password: HTMLInputElement = null;

    componentWillMount() {
        this.setState({isLogin: isLogin()});
    }

    componentDidMount() {
        if (this.userId) {
            this
                .userId
                .focus();
        }
    }

    onKeyUp(e : KeyboardEvent) {
        if (e.keyCode === 13) { // enter
            this.onLoginCall();
        }
    }

    async onLoginCall() {}

    render() {
        if (this.state.isLogin) {
            const {from} = (this.props as any).location.state || {
                from: {
                    pathname: "/"
                }
            };
            return (<Redirect to={from}/>);
        }
        return (
            <div className="app flex-row align-items-center">
                <Container>
                    <Row className="justify-content-center">
                        <Col md="8">
                            <CardGroup>
                                <Card className="p-4">
                                    <CardBody>
                                        <h1>포인트 허브 운영 시스템</h1>
                                        <p className="text-muted">Sign In to your account</p>
                                        <InputGroup className="mb-3">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-user"/>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input
                                                type="text"
                                                placeholder="User ID"
                                                id="userid"
                                                autoComplete="off"
                                                required={true}
                                                innerRef={(input) => this.userId = input}/>
                                        </InputGroup>
                                        <InputGroup className="mb-4">
                                            <InputGroupAddon addonType="prepend">
                                                <InputGroupText>
                                                    <i className="icon-lock"/>
                                                </InputGroupText>
                                            </InputGroupAddon>
                                            <Input
                                                type="password"
                                                placeholder="Password"
                                                id="password"
                                                autoComplete="new-password"
                                                required={true}
                                                onKeyUp={(e : any) => this.onKeyUp(e)}
                                                innerRef={(input) => this.password = input}/>
                                        </InputGroup>
                                        <Row>
                                            <Col xs="6">
                                                <Button color="primary" className="px-4" onClick={() => this.onLoginCall()}>Login</Button>
                                            </Col>
                                            <Col xs="6" className="text-right">관리자 문의 : 이준환(junhwan_lee@sk.com)</Col>
                                        </Row>
                                    </CardBody>
                                </Card>
                            </CardGroup>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

export default Login;
