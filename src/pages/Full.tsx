import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { Container } from 'reactstrap';
import Header from './components/Header';
import Breadcrumb from './components/Breadcrumb';
import Sidebar from './components/Sidebar';
import Footer from './components/Footer';
import { isLogin } from '../reducers/commonStore';
import Login from './Login';

class Full extends Component<{}, {}> {
    constructor( props: any ) {
        super( props );
    }
    
    render() {
        if( !isLogin()) {
            return (<Redirect to={{pathname:"/login"}}/>);
        }

        return (
            <div className="app">
                <Header />
                  <div className="app-body">
                      <Sidebar {...this.props}/>
                      <main className="main">
                          <Breadcrumb/>
                      </main>
                  </div>
                <Footer />
            </div>
        );

    }
}

export default Full;
