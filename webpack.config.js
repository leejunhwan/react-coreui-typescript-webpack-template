const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const extractCSS = new ExtractTextPlugin('[name].fonts.css');
const extractSCSS = new ExtractTextPlugin('[name].styles.css');

// 빌드, 소스 경로
const BUILD_DIR = path.resolve(__dirname, 'build');
const SRC_DIR = path.resolve(__dirname, 'src');

console.log(SRC_DIR)
// 개발 포트 설정
const DEV_LISTEN_PORT = 8000;

module.exports = (env = {}) => {
    return {
        entry: {
            'js/app': ['./src/App.tsx']
        },
        output: {
            path: BUILD_DIR,
            filename: "js/[name].bundle.js",
            chunkFilename: "js/[name].chunk.js"
        },
        // watch: true,
        devtool: env.prod
            ? 'source-map'
            : 'cheap-module-eval-source-map',
        devServer: {
            contentBase: BUILD_DIR,
            port: DEV_LISTEN_PORT,
            // host: DEV_LISTEN_HOST,
            compress: true,
            hot: true,
            open: true,
            https: false
        },
        resolve: {
            extensions: ['.tsx','.ts', '.js', '.json']
        },
        module: {
            rules: [
                {
                    test: /\.(ts|tsx)$/,
                    loader: "tslint-loader",
                    enforce: 'pre',
                    exclude: [/__tests__/, /__mocks__/, /node_modules/]
                }, {
                    test: /\.(ts|tsx)$/,
                    use: [
                        {
                            loader: "ts-loader",
                            options: {
                                // disable type checker - we will use it in fork plugin transpileOnly: true,
                            }
                        }
                    ]
                }, {
                    test: /\.(js|jsx)$/,
                    exclude: /node_modules/,
                    use: {
                        loader: 'babel-loader',
                        options: {
                            cacheDirectory: true,
                            presets: ['react', 'env', 'stage-0']
                        }
                    }
                }, {
                    test: /\.html$/,
                    loader: 'html-loader'
                }, {
                    test: /\.(scss)$/,
                    use: ['css-hot-loader'].concat(extractSCSS.extract({
                        fallback: 'style-loader',
                        use: [
                            {
                                loader: 'css-loader',
                                options: {
                                    alias: {
                                        '../img': '../public/img'
                                    }
                                }
                            }, {
                                loader: 'sass-loader'
                            }
                        ]
                    }))
                }, {
                    test: /\.css$/,
                    use: ['style-loader', 'css-loader'],
                }, {
                    test: /\.(png|jpg|jpeg|gif|ico)$/,
                    use: [
                        {
                            // loader: 'url-loader'
                            loader: 'file-loader',
                            options: {
                                name: './img/[name].[hash].[ext]'
                            }
                        }
                    ]
                }, {
                    test: /\.(ico|png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                    loader: 'url-loader',
                    options: {
                        name: './fonts/[name].[hash].[ext]',
                        limit: 10000,
                      },
                }
            ]
        },
        plugins: [
            new webpack.HotModuleReplacementPlugin(),
            new webpack
                .optimize
                .UglifyJsPlugin({sourceMap: true}),
            new webpack.NamedModulesPlugin(),
            extractCSS,
            extractSCSS,
            new HtmlWebpackPlugin({inject: true, template: 'index.html'}),
            new CopyWebpackPlugin([
                {
                    from: './public/img',
                    to: 'img'
                }
            ], {copyUnmodified: false})
        ]
    }
};
